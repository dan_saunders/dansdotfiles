curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cp newVimRC.vim ~/.vimrc
# From vim, do :PlugInstall
vim -c :PlugInstall
