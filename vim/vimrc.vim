" Line numbers (relative and abs on current line)
set relativenumber
set number

" Tabbing options: show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
set colorcolumn=80

" Colors for differet windows.
hi StatusLine   ctermfg=15  guifg=#ffffff ctermbg=239 guibg=#5f5f5f cterm=bold gui=bold
hi StatusLineNC ctermfg=249 guifg=#7c7c7c ctermbg=237 guibg=#363636 cterm=none gui=none

call plug#begin()
Plug 'https://github.com/vim-scripts/ReplaceWithRegister'
Plug 'https://github.com/ap/vim-buftabline'
Plug 'scrooloose/nerdcommenter'
call plug#end()

" Quick buffer switching.
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>

" Nicer tabline colors (used for buffers).
:hi TabLineFill ctermfg=DarkGrey
:hi TabLine ctermbg=DarkGrey ctermfg=White

" Toggle for paste mode.
set pastetoggle=<F2>
let g:syntastic_auto_loc_list = 0
nnoremap <C-e> 6<C-e>
nnoremap <C-y> 6<C-y>

" Disalbe arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" Alternative to esc
:imap jk <Esc>
